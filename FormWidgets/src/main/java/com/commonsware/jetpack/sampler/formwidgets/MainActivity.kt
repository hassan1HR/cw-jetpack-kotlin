/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.formwidgets

import android.os.Bundle
import android.util.Log
import android.widget.SeekBar
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "FormWidgets"

class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    icon.setOnClickListener { log(R.string.icon_clicked) }
    button.setOnClickListener { log(R.string.button_clicked) }

    swytch.setOnCheckedChangeListener { _, isChecked ->
      log(if (isChecked) R.string.switch_checked else R.string.switch_unchecked)
    }

    checkbox.setOnCheckedChangeListener { _, isChecked ->
      log(if (isChecked) R.string.checkbox_checked else R.string.checkbox_unchecked)
    }

    radioGroup.setOnCheckedChangeListener { _, checkedId ->
      log(
        when (checkedId) {
          R.id.radioButton1 -> R.string.radiobutton1_checked
          R.id.radioButton2 -> R.string.radiobutton2_checked
          else -> R.string.radiobutton3_checked
        }
      )
    }

    seekbar.setOnSeekBarChangeListener(object :
      SeekBar.OnSeekBarChangeListener {
      override fun onProgressChanged(
        seekBar: SeekBar,
        progress: Int,
        fromUser: Boolean
      ) {
        val msg = getString(R.string.seekbar_changed, progress)

        log.text = msg
        Log.d(TAG, msg)
      }

      override fun onStartTrackingTouch(seekBar: SeekBar) {
        // unused
      }

      override fun onStopTrackingTouch(seekBar: SeekBar) {
        // unused
      }
    })
  }

  private fun log(@StringRes msg: Int) {
    log.setText(msg)
    Log.d(TAG, getString(msg))
  }
}
